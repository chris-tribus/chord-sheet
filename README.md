[![Netlify Status](https://api.netlify.com/api/v1/badges/5f3118d8-4c11-47e3-9670-7f9e859ead04/deploy-status)](https://app.netlify.com/sites/friendly-colden-ded164/deploys)

# chord-sheet-js

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
