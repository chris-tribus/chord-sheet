import Vue from 'vue';
import App from './App.vue';
import Hammer from 'hammerjs';

Vue.config.productionTip = false

Vue.directive("doubletap", {
  bind: function (el, binding) {
    if (typeof binding.value === "function") {
      const mc = new Hammer(el);
      mc.get("doubletap").set({ taps: 2 });
      mc.on("doubletap", binding.value);
    }
  }
});

Vue.directive("swipe", {
  bind: function (el, binding) {
    if (typeof binding.value === "function") {
      const mc = new Hammer(el);
      mc.get("swipe").set({ pointers: 2 });
      mc.on("swipe", binding.value);
    }
  }
});

new Vue({
  render: h => h(App),
}).$mount('#app')
